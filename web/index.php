<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run(); 

use yii\grid\GridView;

// Para la primera consulta
echo GridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels' => $consulta1,
    ]),
    'columns' => [
        'nombre',
        'dorsal',
    ],
]);

echo GridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels' => $consulta2,
    ]),
    'columns' => [
        'numetapa',
        'longitud',
    ],
]);

echo GridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels' => $consulta3,
    ]),
    'columns' => [
        'nombre',
        'dorsal',
    ],
]);
